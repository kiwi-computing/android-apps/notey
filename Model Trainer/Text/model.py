from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Lambda, ELU, Activation, BatchNormalization
from keras.layers.convolutional import Convolution2D, Cropping2D, ZeroPadding2D, MaxPooling2D
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import SGD, Adam, RMSprop

class Model():
    def __init__(self, load):
        self.load = load
        self.resize_image = self.load.resize_image
    
    def createModel(self):
        # Function to resize image to 64x64
        row, col, ch = 113, 113, 1

        self.model = Sequential()
        self.model.add(ZeroPadding2D((1, 1), input_shape=(row, col, ch)))

        # Resise data within the neural network
        self.model.add(Lambda(self.load.resize_image))  #resize images to allow for easy computation

        # CNN model - Building the model suggested in paper

        self.model.add(Convolution2D(filters= 32, kernel_size =(5,5), strides= (2,2), padding='same', name='conv1')) #96
        self.model.add(Activation('relu'))
        self.model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2), name='pool1'))

        self.model.add(Convolution2D(filters= 64, kernel_size =(3,3), strides= (1,1), padding='same', name='conv2'))  #256
        self.model.add(Activation('relu'))
        self.model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2), name='pool2'))

        self.model.add(Convolution2D(filters= 128, kernel_size =(3,3), strides= (1,1), padding='same', name='conv3'))  #256
        self.model.add(Activation('relu'))
        self.model.add(MaxPooling2D(pool_size=(2,2),strides=(2,2), name='pool3'))


        self.model.add(Flatten())
        self.model.add(Dropout(0.5))

        self.model.add(Dense(512, name='dense1'))  #1024
        self.model.add(Activation('relu'))
        self.model.add(Dropout(0.5))

        self.model.add(Dense(256, name='dense2'))  #1024
        self.model.add(Activation('relu'))
        self.model.add(Dropout(0.5))

        self.model.add(Dense(self.load.num_classes,name='output'))
        self.model.add(Activation('softmax'))  #softmax since output is within 50 classes
        
        self.model.compile(loss='sparse_categorical_crossentropy', optimizer=Adam(), metrics=['accuracy'])

        return self.model