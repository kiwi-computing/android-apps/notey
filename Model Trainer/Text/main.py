from keras.callbacks import ModelCheckpoint
from math import ceil
from load import * 
from model import Model

class Main():
    def __init__(self):
        self.load = Load()
        self.gen = Generator()
        location = self.load.dataLocation
        imageLocation = self.load.imageLocation
        name = self.load.datainfoFileName
        batch_size = self.load.batch_size
        info = self.load.loadInfo(location, name)
        print("Loading")
        dataImage, labels = self.load.loadData(location, info, imageLocation)
        self.train_files, self.validation_files, self.test_files, self.train_targets, self.validation_targets, self.test_targets = self.load.splitData(dataImage, labels)
        model = Model(self.load)
        print("Create Model")
        self.modelTrain = model.createModel()
        # load weights
        self.modelTrain.load_weights("checkpoint/check-16-2.5719.hdf5")
        
    def train(self, epochs = 1):
        print("Training has started")
        #self.gen.generatorTrain(self.train_files, self.train_targets)
        #print(self.gen.__getitem__(2))
        steps_per_epoch = ceil(self.load.image_count/self.load.batch_size)
        #filepath="checkpoint/check-{epoch:02d}-{val_loss:.4f}.hdf5"
        filepath="checkpoint/check-{epoch:02d}.hdf5"
        checkpoint = ModelCheckpoint(filepath= filepath, verbose=1, save_best_only=False)
        callbacks_list = [checkpoint]
        self.modelTrain.save('text-model.h5') 
        #Model fit generator
        history_object = self.modelTrain.fit_generator(generator=self.gen.generatorTrain(self.train_files, self.train_targets),
                                                       epochs=epochs, steps_per_epoch=steps_per_epoch, verbose=1,
                                                       callbacks=callbacks_list)
        self.modelTrain.save('text1-model.h5') 
        self.modelTrain.load_weights('text-model.h5')
        scores = self.modelTrain.evaluate(self.gen.generatorTest(self.test_files, self.test_targets),842) 
        print("Accuracy = ", scores[1])

main = Main()
main.train()