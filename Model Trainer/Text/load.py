from __future__ import division
import numpy as np
import os
import platform
import glob
import sklearn.preprocessing
import sklearn.model_selection
from tqdm import tqdm
import time
from PIL import Image
import random 
import tensorflow as tf

class Load():
    def __init__(self, batch_size = 16, num_classes = 115320, dataLocation = "data/", datainfoFileName = "wordsinfo.txt", imageLocation = "a01/*/*.png"):
        self.dataLocation = dataLocation
        self.datainfoFileName = datainfoFileName
        self.batch_size = batch_size
        self.num_classes = num_classes
        self.imageLocation = imageLocation
        self.image_count = len(list(glob.glob(self.dataLocation+imageLocation)))
    
    def loadInfo(self, Location, FileName):
        d = {}
        with open(Location + FileName, "rt") as f:
            for line in f:
                key = line.split(' ')[0]
                writer = line.split(' ')[8]
                d[key] = writer
        #print(len(d.keys()))
        return d
    
    def loadData(self, Location, d, imageLocation):
        tmp = []
        target_list = []

        path_to_files = os.path.join(Location, imageLocation)
        for fileLocation in tqdm(sorted(glob.glob(path_to_files)), ascii=True, desc="Load"):
            tmp.append(fileLocation)
            file, ext = os.path.splitext(fileLocation)
            if (platform.system() == 'Linux'):
                parts = file.split('/')
            elif (platform.system() == 'Windows'):
                parts = file.split('\\')
            form = parts[3]
            for key in d:
                if key == form:
                    target_list.append(str(d[form]))

        img_files = np.asarray(tmp)
        img_targets = np.asarray(target_list)
        #print(img_files.shape)
        #print(img_targets.shape)
        encoder = sklearn.preprocessing.LabelEncoder()
        encoder.fit(img_targets)
        encoded_Y = encoder.transform(img_targets) #Transform non-numerical labels to numerical labels.

        return img_files, encoded_Y
    
    def splitData(self, img_files, encoded_Y):
        train_files, rem_files, train_targets, rem_targets = sklearn.model_selection.train_test_split(
        img_files, encoded_Y, train_size=0.3, random_state=52, shuffle= False)

        validation_files, test_files, validation_targets, test_targets = sklearn.model_selection.train_test_split(
                rem_files, rem_targets, train_size=0.3, random_state=22, shuffle=False)

        return train_files, validation_files, test_files, train_targets, validation_targets, test_targets
    
    def resize_image(self, image):
        # resize the image to the desired size.
        return tf.image.resize(image,[56,56])

class Generator(tf.keras.utils.Sequence):
    def __init__(self):
        self.load = Load()
        self.batch_size = self.load.batch_size
    
    def __len__(self):
        return int(self.load.image_count/self.batch_size)+1
    
    def __getitem__(self,idx):
        offset = idx*self.batch_size
        batch_samples = self.samples[offset:offset + self.batch_size]
        batch_targets = self.target_files[offset:offset + self.batch_size]
        return self.generateBatch(batch_samples,batch_targets)

    def generateBatch(self, batch_samples, batch_targets):
        num_samples = len(batch_samples)
        images = []
        targets = []
        #print(batch_samples)
        #print(batch_targets)
        for i in range(len(batch_samples)):
            #print(self.processimage(batch_samples,batch_targets, i, images, targets))
            self.processimage(batch_samples,batch_targets, i, images, targets)
            
        # trim image to only see section with road
        x_train = np.array(images)
        y_train = np.array(targets)
        #print(x_train)
        # The 1./255 is to convert from uint8 to float32 in range [0,1].
        x_train = x_train.reshape(x_train.shape[0], 113, 113, 1)
        #convert to float and normalize
        x_train = x_train.astype('float32')
        x_train /= 255
        
        #print(np.isnan(x_train))
        #print(np.isnan(y_train))
        if (x_train.size != 0 or y_train.size != 0 ):
            return x_train, y_train
            
    def batchLocation(self):
        for offset in tqdm(range(0, num_samples, batch_size), ascii=True, desc="Batch"):
            batch_samples = samples[offset:offset + batch_size]
            batch_targets = target_files[offset:offset + Load().batch_size]
            yield batch_samples, batch_targets
            
    def processimage(self,batch_samples,batch_targets, i, images, targets):
        batch_sample = batch_samples[i]
        batch_target = batch_targets[i]
        im = Image.open(batch_sample)
        cur_width = im.size[0]
        cur_height = im.size[1]
        
        height_fac = 113 / cur_height

        new_width = int(cur_width * height_fac)
        size = new_width, 113
        imresize = im.resize((size), Image.ANTIALIAS)  # Resize so height = 113 while keeping aspect ratio
        now_width = imresize.size[0]

        avail_x_points = list(range(0, now_width - 113 ))# total x start points are from 0 to width -113

        # Pick random x%
        pick_num = int(len(avail_x_points)*self.factor)
        
        # Now pick
        random_startx = random.sample(avail_x_points,  pick_num)

        for start in random_startx:
            imcrop = imresize.crop((start, 0, start+113, 113))
            #print(imcrop)
            #print(batch_target)
            images.append(np.asarray(imcrop))
            targets.append(batch_target)
    
    def generatorTrain(self, train_files, train_targets):
        self.factor = 0.3
        self.samples = train_files
        self.target_files = train_targets
        return self
    
    def generatorValidation(self, validation_files, validation_targets):
        self.factor = 0.3
        self.samples = validation_files
        self.target_files = validation_targets
        return self
    
    def generatorTest(self, test_files, test_targets):
        self.factor = 0.1
        self.samples = test_files
        self.target_files = test_targets
        return self
