package dk.kiwicomputing.notey;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    /** Called when the user taps the button */
    public void Camera(View view) {
        Intent intent = new Intent(this, TextRecognitionActivity.class);
        startActivity(intent);
    }
}

