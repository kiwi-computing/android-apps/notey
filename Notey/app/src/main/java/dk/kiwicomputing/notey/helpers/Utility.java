package dk.kiwicomputing.notey.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import dk.kiwicomputing.notey.R;

public class Utility {

    public String tempImage(byte[] bytes) throws FileNotFoundException {

        String ts = Long.toString(System.currentTimeMillis() / 1000);
        final File file = new File(Environment.getExternalStorageDirectory()+ "/");
        File dest = null;
        FileOutputStream outputStream = null;
        boolean success = true;
        if (!file.exists()) {
            success = file.mkdir();
        }
        if (success) {
            dest = new File(file,ts+".png");
            try  {
                outputStream = new FileOutputStream(dest);
               outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (outputStream != null) {
                        outputStream.close();
                        Log.d("tempImage", "OK!!");
                    }
                } catch (IOException e) {
                    Log.d("tempImage", e.getMessage() + "Error");
                    e.printStackTrace();
                }
            }
        }
        return String.valueOf(dest);
    }
    public void saveImage(Mat roi, String dst) {
        Imgcodecs.imwrite(dst, roi);
    }

    public Mat imageToMat(String img){
        Mat mat = Imgcodecs.imread(img);
        return mat;
    }

    public static Bitmap rotateImage(Bitmap source, float angle)
    {
        Bitmap retVal;

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        retVal = Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                source.getHeight(), matrix, true);

        return retVal;
    }

    public static Bitmap fixImageOrientation(Bitmap bitmap, String imgUri)
    {
        Bitmap finalBitmap = null;

        ExifInterface ei = null;
        try
        {
            ei = new ExifInterface(imgUri);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                finalBitmap = Utility.rotateImage(bitmap, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                finalBitmap = Utility.rotateImage(bitmap, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                finalBitmap = Utility.rotateImage(bitmap, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                finalBitmap = Utility.rotateImage(bitmap, 270);
                break;
            default:
                finalBitmap = bitmap;
                break;
        }

        return finalBitmap;
    }


    public static Bitmap getBitmapFromMat(Mat tempMat)
    {
        Bitmap bmp = null;
        Mat tmp = new Mat(tempMat.rows(), tempMat.cols(), CvType.CV_8U,
                new Scalar(4));
        try
        {
            // Imgproc.cvtColor(seedsImage, tmp, Imgproc.COLOR_RGB2BGRA);
            Imgproc.cvtColor(tempMat, tmp, Imgproc.COLOR_GRAY2RGBA, 4);
            bmp = Bitmap.createBitmap(tmp.cols(), tmp.rows(),
                    Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(tmp, bmp);

        }
        catch (CvException e)
        {
            Log.d("Exception", e.getMessage());
        }

        return bmp;
    }


    /** Create a File for saving an image or video */
    public static File getOutputMediaFile(Context mContext)
    {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                mContext.getString(R.string.app_name));

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists())
        {
            if (!mediaStorageDir.mkdirs())
            {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    public static void showToast(Context mContext , String msg)
    {
        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
    }
}
