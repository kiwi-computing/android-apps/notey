package dk.kiwicomputing.notey.api;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import com.microsoft.graph.authentication.IAuthenticationProvider; //Imports the Graph sdk Auth interface
import com.microsoft.graph.concurrency.ICallback;
import com.microsoft.graph.core.ClientException;
import com.microsoft.graph.http.IHttpRequest;
import com.microsoft.graph.models.extensions.*;
import com.microsoft.graph.options.HeaderOption;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.requests.extensions.GraphServiceClient;
import com.microsoft.graph.requests.extensions.INotebookCollectionPage;
import com.microsoft.graph.requests.extensions.IOnenoteSectionCollectionPage;
import com.microsoft.identity.client.AuthenticationCallback; // Imports MSAL auth methods
import com.microsoft.identity.client.*;
import com.microsoft.identity.client.exception.*;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dk.kiwicomputing.notey.R;

public class OneNoteActivity extends Activity {
    private final static String[] SCOPES = {"email"/*View email*/, "Notes.Create" /*Create pages*/, "Notes.Read", "Notes.Read.All" /*Read pages*/, "Notes.ReadWrite" , "Notes.ReadWrite.All" /*Read and write pages*/};
    /* Azure AD v2 Configs */
    final static String AUTHORITY = "https://login.microsoftonline.com/common";
    private ISingleAccountPublicClientApplication mSingleAccountApp;
    private String fullText = "";
    private static final String TAG = OneNoteActivity.class.getSimpleName();

    private Pattern NamePattern = Pattern.compile("(?<=\"displayName\":\")[\\w0-9 ]+(?=\")");
    private String NotebookID = "";
    private String sectionID = "";
    private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

    /* UI & Debugging Variables */
    Button signInButton;
    Button signOutButton;
    Button callGraphApiInteractiveButton;
    Button callGraphApiSilentButton;
    TextView logTextView;
    TextView currentUserTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullText = getIntent().getStringExtra("fullString");
        setContentView(R.layout.activity_onenote);

        initializeUI();

        PublicClientApplication.createSingleAccountPublicClientApplication(getApplicationContext(),
                R.raw.auth_configbn_single_account, new IPublicClientApplication.ISingleAccountApplicationCreatedListener() {
                    @Override
                    public void onCreated(ISingleAccountPublicClientApplication application) {
                        mSingleAccountApp = application;
                        loadAccount();
                    }
                    @Override
                    public void onError(MsalException exception) {
                        displayError(exception);
                    }
                });
    }

    //When app comes to the foreground, load existing account to determine if user is signed in
    private void loadAccount() {
        if (mSingleAccountApp == null) {
            return;
        }

        mSingleAccountApp.getCurrentAccountAsync(new ISingleAccountPublicClientApplication.CurrentAccountCallback() {
            @Override
            public void onAccountLoaded(@Nullable IAccount activeAccount) {
                // You can use the account data to update your UI or your app database.
                updateUI(activeAccount);
            }

            @Override
            public void onAccountChanged(@Nullable IAccount priorAccount, @Nullable IAccount currentAccount) {
                if (currentAccount == null) {
                    // Perform a cleanup task as the signed-in account changed.
                    performOperationOnSignOut();
                }
            }

            @Override
            public void onError(@NonNull MsalException exception) {
                displayError(exception);
            }
        });
    }

    private void initializeUI(){
        signInButton = findViewById(R.id.signIn);
        callGraphApiSilentButton = findViewById(R.id.callGraphSilent);
        callGraphApiInteractiveButton = findViewById(R.id.callGraphInteractive);
        signOutButton = findViewById(R.id.clearCache);
        logTextView = findViewById(R.id.txt_log);
        currentUserTextView = findViewById(R.id.current_user);

        //Sign in user
        signInButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                if (mSingleAccountApp == null) {
                    return;
                }
                mSingleAccountApp.signIn(OneNoteActivity.this, null, SCOPES, getAuthInteractiveCallback());
            }
        });

        //Sign out user
        signOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSingleAccountApp == null){
                    return;
                }
                mSingleAccountApp.signOut(new ISingleAccountPublicClientApplication.SignOutCallback() {
                    @Override
                    public void onSignOut() {
                        updateUI(null);
                        performOperationOnSignOut();
                    }
                    @Override
                    public void onError(@NonNull MsalException exception){
                        displayError(exception);
                    }
                });
            }
        });

        //Interactive
        callGraphApiInteractiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSingleAccountApp == null) {
                    return;
                }
                mSingleAccountApp.acquireToken(OneNoteActivity.this, SCOPES, getAuthInteractiveCallback());
            }
        });

        //Silent
        callGraphApiSilentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSingleAccountApp == null){
                    return;
                }
                mSingleAccountApp.acquireTokenSilentAsync(SCOPES, AUTHORITY, getAuthSilentCallback());
            }
        });
    }

    private AuthenticationCallback getAuthInteractiveCallback() {
        return new AuthenticationCallback() {
            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                /* Successfully got a token, use it to call a protected resource - MSGraph */
                Log.d(TAG, "Successfully authenticated");
                /* Update UI */
                updateUI(authenticationResult.getAccount());
                /* call graph */
                callGraphAPI(authenticationResult);
            }

            @Override
            public void onError(MsalException exception) {
                /* Failed to acquireToken */
                Log.d(TAG, "Authentication failed: " + exception.toString());
                displayError(exception);
            }
            @Override
            public void onCancel() {
                /* User canceled the authentication */
                Log.d(TAG, "User cancelled login.");
            }
        };
    }

    private SilentAuthenticationCallback getAuthSilentCallback() {
        return new SilentAuthenticationCallback() {
            @Override
            public void onSuccess(IAuthenticationResult authenticationResult) {
                Log.d(TAG, "Successfully authenticated");
                callGraphAPI(authenticationResult);
            }
            @Override
            public void onError(MsalException exception) {
                Log.d(TAG, "Authentication failed: " + exception.toString());
                displayError(exception);
            }
        };
    }

    private void callGraphAPI(IAuthenticationResult authenticationResult) {

        final String accessToken = authenticationResult.getAccessToken();

        final IGraphServiceClient graphClient =
                GraphServiceClient
                        .builder()
                        .authenticationProvider(new IAuthenticationProvider() {
                            @Override
                            public void authenticateRequest(IHttpRequest request) {
                                Log.d(TAG, "Authenticating request," + request.getRequestUrl());
                                request.addHeader("Authorization", "Bearer " + accessToken);
                            }
                        })
                        .buildClient();
        setNotebookID(graphClient);
    }

    private void setNotebookID(final IGraphServiceClient graphClient) {
        graphClient.me().onenote().notebooks()
                .buildRequest()
                .get(new ICallback<INotebookCollectionPage>() {
                    int i = 0;
                    int l = 0;
                    @Override
                    public void success(INotebookCollectionPage notebooks ) {
                        Log.d(TAG, "Found Notebook " + notebooks);
                        displayGraphResult(notebooks.getRawObject());
                        JsonArray array = notebooks.getRawObject().getAsJsonObject().getAsJsonArray("value");
                        int arraySize = array.size();
                        while (i < arraySize)
                        {
                            //Log.d("Debug", array.get(i).getAsJsonObject().get("displayName").toString());
                            //Log.d("Debug", String.valueOf(array.get(i).getAsJsonObject().get("displayName").toString().equals("\"Notey\"")));
                            if (array.get(i).getAsJsonObject().get("displayName").toString().equals("\"Notey\"")) {
                                i = 0;
                                break;
                            }
                            i++;
                            l++;
                        }
                        if (i != 0){
                            Log.d("Debug", "Didn't find the notebook Notey, creating it");
                            createNoteBook(graphClient);
                        }
                        if (i == 0) {
                            Log.d("Debug", String.valueOf(array.get(l).getAsJsonObject().get("id")));
                            int size = array.get(l).getAsJsonObject().get("id").toString().length();
                            NotebookID = array.get(l).getAsJsonObject().get("id").toString().substring(1,size-1);
                            String rawhtml = ("<!DOCTYPE html>\n" +
                                    "<html>\n" +
                                    "  <head>\n" +
                                    "    <title>Scan - "+LocalDateTime.now()+"</title>\n" +
                                    "    <meta name=\"created\" content=\"" + LocalDateTime.now() + "\" />\n" +
                                    "  </head>\n" +
                                    "  <body>\n" +
                                    "    <p>"+fullText+"</p>\n" +
                                    "  </body>\n" +
                                    "</html>");
                            setSectionID(graphClient, "Info", rawhtml);
                        }
                    }

                    @Override
                    public void failure(ClientException ex) {
                        displayError(ex);
                    }
                });
    }

    private void createNoteBook(IGraphServiceClient graphClient) {
        Notebook notebook = new Notebook();
        notebook.displayName = "Notey";

        graphClient.me().onenote().notebooks()
                .buildRequest()
                .post(notebook);
        setNotebookID(graphClient);
    }

    private void createSection(IGraphServiceClient graphClient, String name){
        OnenoteSection onenoteSection = new OnenoteSection();
        onenoteSection.displayName = name;

        graphClient.me().onenote().notebooks(NotebookID).sections()
                .buildRequest()
                .post(onenoteSection);
        String rawhtml = ("<!DOCTYPE html>\n" +
                "<html>\n" +
                "  <head>\n" +
                "    <title>Info</title>\n" +
                "    <meta name=\"created\" content=\"" + LocalDateTime.now() + "\" />\n" +
                "  </head>\n" +
                "  <body>\n" +
                "    <p>This page contains some <i>formatted</i> <b>text</b> and an image.</p>\n" +
                "<img src=\"../res/mipmap-xxxhdpi/ic_launcher.png\" alt=\"Logo\" width=\"500\" />" +
                "  </body>\n" +
                "</html>");
        setSectionID(graphClient, name, rawhtml);

    }
    private void setSectionID(final IGraphServiceClient graphClient, final String name, final String html) {
        graphClient.me().onenote().notebooks(NotebookID).sections()
                .buildRequest()
                .get(new ICallback<IOnenoteSectionCollectionPage>() {
                    int i = 0;
                    int l = 0;
                    @Override
                    public void success(IOnenoteSectionCollectionPage sections) {
                        Log.d(TAG, "Found Notebook " + sections);
                        displayGraphResult(sections.getRawObject());
                        JsonArray array = sections.getRawObject().getAsJsonObject().getAsJsonArray("value");
                        int arraySize = array.size();
                        if (arraySize == 0){
                            createSection(graphClient,name);
                            array = getSections(graphClient).getRawObject().getAsJsonObject().getAsJsonArray("value");
                            arraySize = array.size();
                        }
                        while (i < arraySize)
                        {
                            Log.d("Debug", array.get(i).getAsJsonObject().get("displayName").toString());
                            Log.d("Debug", String.valueOf(array.get(i).getAsJsonObject().get("displayName").toString().equals("\"Info\"")));
                            if (array.get(i).getAsJsonObject().get("displayName").toString().equals("\"Info\"")) {
                                i = 0;
                                break;
                            }
                            i++;
                            l++;
                        }
                        if (i != 0){
                            Log.d("Debug", "Didn't find the section Info, creating it");
                            createSection(graphClient,name);
                        }
                        if (i == 0) {
                            Log.d("Debug", "l: "+String.valueOf(l));
                            Log.d("Debug", String.valueOf(sections.getRawObject().getAsJsonObject()));

                            Log.d("Debug", "array: "+String.valueOf(array));
                            Log.d("Debug", "size: " + String.valueOf(array.size()));
                            Log.d("Debug", String.valueOf(array.get(l).getAsJsonObject().get("id")));
                            int size = array.get(l).getAsJsonObject().get("id").toString().length();
                            sectionID = array.get(l).getAsJsonObject().get("id").toString().substring(1,size-1);
                            Log.e("ID", sectionID);
                            postContent(html, graphClient);
                        }
                    }

                    @Override
                    public void failure(ClientException ex) {
                        displayError(ex);
                    }
                });
    }

    public void postContent(String content, IGraphServiceClient graphClient) {
        //This sets up the multipart request
        Multipart multipart = new Multipart();

        try {
            //Add all the files to the multipart request
            multipart.addHtmlPart("Presentation", content.getBytes());

            // Add multipart request header
            List<Option> options = new ArrayList<Option>();
            options.add(new HeaderOption(
                    "Content-Type", "multipart/form-data; boundary=\"" + multipart.getBoundary() + "\""
            ));

            // Post the multipart content
            graphClient.me()
                    .onenote()
                    .sections(sectionID)
                    .pages()
                    .buildRequest(options)
                    .post(multipart.content());
        } catch (Exception e) {
            Log.e("Error",e.toString());
            e.printStackTrace();
        }
    }

    private Notebook getNoteBook(IGraphServiceClient graphClient) {
        return graphClient.me().onenote().notebooks(NotebookID)
                .buildRequest()
                .get();
    }
    private OnenoteSection getSection(IGraphServiceClient graphClient) {
        return graphClient.me().onenote().sections(sectionID)
                .buildRequest()
                .get();
    }
    private IOnenoteSectionCollectionPage getSections(IGraphServiceClient graphClient) {
        return graphClient.me().onenote().notebooks(NotebookID).sections()
                .buildRequest()
                .get();
    }

    private void updateUI(@Nullable final IAccount account) {
        if (account != null) {
            signInButton.setEnabled(false);
            signOutButton.setEnabled(true);
            callGraphApiInteractiveButton.setEnabled(true);
            callGraphApiSilentButton.setEnabled(true);
            currentUserTextView.setText(account.getUsername());
        } else {
            signInButton.setEnabled(true);
            signOutButton.setEnabled(false);
            callGraphApiInteractiveButton.setEnabled(false);
            callGraphApiSilentButton.setEnabled(false);
            currentUserTextView.setText("");
            logTextView.setText("");
        }
    }

    private void displayError(@NonNull final Exception exception) {
        logTextView.setText(exception.toString());
        Log.e("displayError", exception.toString());

    }

    private void displayGraphResult(@NonNull final JsonObject graphResponse) {
        logTextView.setText(graphResponse.toString());
        Log.e("displayGraphResult", graphResponse.toString());
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    private void performOperationOnSignOut() {
        final String signOutText = "Signed Out.";
        currentUserTextView.setText("");
        Toast.makeText(getApplicationContext(), signOutText, Toast.LENGTH_SHORT)
                .show();
    }


}
