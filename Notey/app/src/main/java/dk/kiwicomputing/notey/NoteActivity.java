package dk.kiwicomputing.notey;

import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import org.opencv.core.Mat;

public class NoteActivity extends AppCompatActivity {
    public static Mat imageMat;
    private Image image;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);
        // Button holder
        button = findViewById(R.id.cameraButton);
        // View holder
        ImageView imageView = findViewById(R.id.imageNote);
        imageView.setImageBitmap(Bitmap.createBitmap(imageMat.cols(), imageMat.rows(),Bitmap.Config.ARGB_8888));
    }
}
